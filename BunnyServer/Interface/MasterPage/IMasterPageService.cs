﻿using Framework.Dependency_Injection.ClassHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunnyServer.Interface.MasterPage
{
    public interface IMasterPageService : IPerRequestDependency
    {
        void testDI();
    }
}
