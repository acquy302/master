﻿using BunnyBussiness.Interface.MasterPage;
using BunnyServer.Interface.MasterPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunnyServer.Implement.MasterPage
{
    public class MasterPageService : IMasterPageService
    {
        private IMasterPageBusiness _masterPageBusiness;
        public MasterPageService(IMasterPageBusiness masterPageBusiness)
        {
            _masterPageBusiness = masterPageBusiness;
        }
        public void testDI()
        {
            _masterPageBusiness.testDI();
            //throw new NotImplementedException();
        }
    }
}
