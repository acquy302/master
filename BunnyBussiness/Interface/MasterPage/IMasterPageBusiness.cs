﻿using Framework.Dependency_Injection.ClassHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunnyBussiness.Interface.MasterPage
{
    public interface IMasterPageBusiness : IPerRequestDependency
    {
        void testDI();
    }
}
