﻿using AutoMapper;
using BunnyEntity.Entity;
using BunnyModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunnyModel.Profile
{
    public class UserModelProfile : AutoMapper.Profile
    {
        public UserModelProfile()
        {
            CreateMap<UserManagerment, UserManagermentModel>()
                .ForMember(m => m.Id, map => map.MapFrom(m => m.Id))
                .ForMember(m => m.UserName, map => map.MapFrom(m => m.UserName))
                .ForMember(m => m.PassWord, map => map.MapFrom(m => m.PassWord));
            CreateMap<UserManagermentModel, UserManagerment>()
                .ForMember(m => m.Id, map => map.MapFrom(m => m.Id))
                .ForMember(m => m.UserName, map => map.MapFrom(m => m.UserName))
                .ForMember(m => m.PassWord, map => map.MapFrom(m => m.PassWord));

        }
    }
}
