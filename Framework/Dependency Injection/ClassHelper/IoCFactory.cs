﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Framework.Dependency_Injection.ClassHelper
{
    public sealed class IoCFactory : IDisposable
    {
        #region Singleton

        /// <summary>
        ///     Get singleton instance of <see cref="IoCFactory" /> 
        /// </summary>
        public static IoCFactory Instance { get; } = new IoCFactory();

        #endregion Singleton 

        /// <summary>
        ///     Get current configured IContainer <remarks> At <see langword="this" /> moment only
        ///     <see cref="IoCUnityContainer" /> exists </remarks>
        /// </summary>
        public IoCUnityContainer CurrentContainer { get; }

        /// <summary>
        ///     Get <see langword="object" /> instance 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetObjectInstance<T>() where T : class
        {
            try
            {
                Debug.WriteLine(typeof(T));
                return Instance.CurrentContainer.Container.Resolve(typeof(T)) as T;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e.InnerException);
            }
        }

        #region ctor

        private IoCFactory()
        {
            CurrentContainer = new IoCUnityContainer();
        }

        #endregion ctor

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        ///     detect if already disposed 
        /// </summary>
        private bool _disposed;

        /// <summary>
        ///     Destructor 
        /// </summary>
        ~IoCFactory()
        {
            Dispose(false);
        }

        /// <summary>
        ///     Dispose 
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (CurrentContainer != null)
                        CurrentContainer.Dispose();
                }
            }
            _disposed = true;
        }
        
    }
}
