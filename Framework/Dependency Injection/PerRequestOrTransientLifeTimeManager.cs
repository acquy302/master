﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.AspNet.Mvc;
using Unity.Lifetime;

namespace Framework.Dependency_Injection
{
    public class PerRequestOrTransientLifeTimeManager : LifetimeManager
    {
        private readonly PerRequestLifetimeManager _perRequestLifetimeManager = new PerRequestLifetimeManager();
        private readonly TransientLifetimeManager _transientLifetimeManager = new TransientLifetimeManager();

        private LifetimeManager GetAppropriateLifetimeManager()
        {
            if (System.Web.HttpContext.Current == null)
                return _transientLifetimeManager;

            return _perRequestLifetimeManager;
        }

        //public override object GetValue()
        //{
        //    return GetAppropriateLifetimeManager().GetValue();
        //}

        //public override void SetValue(object newValue)
        //{
        //    GetAppropriateLifetimeManager().SetValue(newValue);
        //}

        //public override void RemoveValue()
        //{
        //    GetAppropriateLifetimeManager().RemoveValue();
        //}

        public override object GetValue(ILifetimeContainer container = null)
        {
            return GetAppropriateLifetimeManager().GetValue();
        }

        protected override LifetimeManager OnCreateLifetimeManager()
        {
            return new PerRequestOrTransientLifeTimeManager();
        }
    }
}
