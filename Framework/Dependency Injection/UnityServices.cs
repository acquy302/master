﻿using Framework.Dependency_Injection.ClassHelper;
using Framework.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Unity;
//using Unity.RegistrationByConvention;

namespace Framework.Dependency_Injection
{
    /// <summary>
    ///     Unity Services Helper 
    /// </summary>
    public static class UnityServices
    {
        public static List<Type> ListMarkDependencyInterface => new List<Type>
        {
            //typeof(ISingletonDependency),
            typeof(IPerRequestDependency),
            //typeof(IPerResolveDependency),
            //typeof(IDependency)
        };
        /// <summary>
        ///     Auto mapping interfaces and object 
        /// </summary>
        /// <param name="container"> </param>
        /// <param name="assemblies"></param>
        public static void AutoRegister(IUnityContainer container, IList<Assembly> assemblies = null)
        {
            
            Validate(container);

            if (assemblies == null)
            {
                assemblies = AssemblyHelper.GetAssemblies();
            }

            IEnumerable<Type> listClass = assemblies.GetClassesFromAssemblies();

            RegisterTypesWithConvention(container, listClass);
        }

        /// <summary>
        ///     Register types with convention Interface must inheritance <see cref="IDependency" />/ <see cref="IPerRequestDependency" />/ <see cref="ISingletonDependency" /> 
        /// </summary>
        /// <param name="container"></param>
        /// <param name="listClass"></param>
        public static void RegisterTypesWithConvention(IUnityContainer container, IEnumerable<Type> listClass)
        {
            Validate(container);
            listClass = listClass.Distinct().ToList();

            foreach (Type @class in listClass)
            {
                Type @localClass = @class;
                var listInterface = @class.GetInterfacesToBeRegistered();

                // Not allow class direct inject to mark dependency interface
                listInterface = listInterface
                    .Where(x => x.IsGenericType == localClass.IsGenericType && !ListMarkDependencyInterface.Contains(x));

                foreach (Type @interface in listInterface)
                {
                    //if (typeof(ISingletonDependency).IsAssignableFrom(@interface))
                    //{
                    //    // Type extend from ISingletonDependency interface, so register it as Singleton
                    //    container.RegisterType(@interface, @localClass, new ContainerControlledLifetimeManager());
                    //    break;
                    //}

                    if (typeof(IPerRequestDependency).IsAssignableFrom(@interface))
                    {
                        // Type extend from IPerRequestDependency interface, so register it as Instant per Request
                        container.RegisterType(@interface, @localClass, new PerRequestOrTransientLifeTimeManager());
                        break;
                    }

                    //if (typeof(IPerResolveDependency).IsAssignableFrom(@interface))
                    //{
                    //    // Type extend from IDependency interface, so register it as Instant per Dependency
                    //    container.RegisterType(@interface, @localClass, new PerResolveLifetimeManager());
                    //    break;
                    //}

                    //if (typeof(IDependency).IsAssignableFrom(@interface))
                    //{
                    //    // Type extend from IDependency interface, so register it as Instant Normal
                    //    container.RegisterType(@interface, @localClass, new TransientLifetimeManager());
                    //    break;
                    //}
                }
            }

            //PrintRegisterInformation(container);
        }



        /// <summary>
        ///     Validate for Unity Framework 
        /// </summary>
        /// <param name="container"></param>
        private static void Validate(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException(nameof(container));
            }
        }
    }
}
