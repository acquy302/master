﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Unity.RegistrationByConvention;

namespace Framework.Helper
{
    public static class AssemblyHelper
    {
        public static IList<Assembly> GetAssemblies()
        {
            // all assemblies in assemblies resolver folder
            IList<Assembly> assemblies = new List<Assembly>();
            string path = Path.GetFullPath(AppDomain.CurrentDomain.RelativeSearchPath);
            List<string> dllFilesFullPath = Directory.GetFiles(path, searchPattern: "*.dll").ToList();

            // loaded assemblies
            Assembly[] loadedAssemblies =
                AppDomain.CurrentDomain
                .GetAssemblies();

            List<string> loadedAssembliesFullPathWithoutExtension =
                loadedAssemblies
                .Select(x => Path.GetFileNameWithoutExtension(x.CodeBase))
                .ToList();

            // check to get assemblies is loaded, get and load assemblies not loaded
            foreach (var dll in dllFilesFullPath)
            {
                string dllWithoutExtension = Path.GetFileNameWithoutExtension(dll);

                // if already loaded, then add else, load and add that assembly to list
                if (loadedAssembliesFullPathWithoutExtension.Contains(dllWithoutExtension))
                {
                    Assembly loadedAssembly =
                        loadedAssemblies.FirstOrDefault(
                            x => dllWithoutExtension != null && x.FullName.Contains(dllWithoutExtension));

                    if (loadedAssembly != null)
                    {
                        assemblies.Add(loadedAssembly);
                    }
                }
                else
                {
                    assemblies.Add(Assembly.LoadFile(dll));
                }
            }
            return assemblies;
        }
        public static IEnumerable<Type> GetClassesFromAssemblies(this IEnumerable<Assembly> assemblies)
        {
            var allClasses =
                assemblies != null
                ? AllClasses.FromAssemblies(assemblies)
                : AllClasses.FromAssembliesInBasePath();

            return allClasses
                .Where(n => n.Namespace != null && !n.IsInterface)
                .Select(i => i.IsGenericType ? i.GetGenericTypeDefinition() : i)
                .ToList();
        }
        /// <summary>
        ///     Get interfaces to be registered 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="TargetInvocationException">
        ///     A static initializer is invoked and throws an exception.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="interfaces" /> or <paramref name="type" /> is null.
        /// </exception>
        public static IEnumerable<Type> GetInterfacesToBeRegistered(this Type type)
        {
            var allInterfacesOnType =
                type
                .GetInterfaces()
                .Select(i => i.IsGenericType ? i.GetGenericTypeDefinition() : i)
                .ToList();

            return allInterfacesOnType;
        }
    }
}
