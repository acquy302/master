﻿using BunnyServer.Interface.MasterPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bunny.Controllers
{
    public class MasterPageController : Controller
    {
        private IMasterPageService _masterPageService;

        public MasterPageController(IMasterPageService masterPageService)
        {
            _masterPageService = masterPageService;
        }
        // GET: MasterPage
        public ActionResult Index()
        {
            _masterPageService.testDI();
            return View();
        }
        [HttpPost]       
        public ActionResult Login(string userName, string password)
        {
            //Log.submit(json);
            return null;
        }
    }
}