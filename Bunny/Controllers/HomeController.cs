﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BunnyEntities;
using BunnyEntity.Entity;
using BunnyModel.Models;
namespace Bunny.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            var dbContext = new BunnyEntityContext();
            using (dbContext)
            {
                var users = from user in dbContext.UserManagerments select user;
                var model = Mapper.Map<List<UserManagerment>, List<UserManagermentModel>>(users.ToList());
            }
                
                ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}