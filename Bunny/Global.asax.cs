﻿using AutoMapper;
using AutoMapper.Configuration;
using BunnyModel.Profile;
using Framework.Dependency_Injection;
using Framework.Dependency_Injection.ClassHelper;
using Framework.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity;

namespace Bunny
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //Register mapper
            AutoRegisterConfiguration();
        }
        private void AutoRegisterConfiguration()
        {
            //Define
            //IUnityContainer container = new UnityContainer();
            IUnityContainer unityContainer = IoCFactory.Instance.CurrentContainer.Container;
            //Register mapper
            AutoMapperConfiguration.Configure();
            //Register DI
            
            UnityServices.AutoRegister(unityContainer);

        }
        private class AutoMapperConfiguration 
        {
            public static void Configure()
            {
                //Get all profile
                List<Type> profiles =
                AssemblyHelper
                    .GetAssemblies()
                    .GetClassesFromAssemblies()
                    .Where(x =>
                        typeof(Profile).IsAssignableFrom(x)
                        || typeof(Profile).IsAssignableFrom(x))
                    .Distinct()
                    .ToList();
                //
                Mapper.Initialize(x =>
                {
                    foreach (var profile in profiles)
                    {
                        x.AddProfile(profile);
                    }
                    
                });

                Mapper.Configuration.AssertConfigurationIsValid();
            }
            
        }
        private static void GetAllConfigAutoMapper()
        {
            List<Type> profiles =
                AssemblyHelper
                    .GetAssemblies()
                    .GetClassesFromAssemblies()
                    .Where(x =>
                        typeof(Profile).IsAssignableFrom(x)
                        || typeof(Profile).IsAssignableFrom(x))
                    .Distinct()
                    .ToList();
        }
    }
}
