﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartupAttribute(typeof(Bunny.Startup))]
namespace Bunny
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
